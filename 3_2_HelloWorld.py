#%%
# Copyright 2015 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

print(mnist.train.images.shape, mnist.train.labels.shape)
print(mnist.test.images.shape, mnist.test.labels.shape)
print(mnist.validation.images.shape, mnist.validation.labels.shape)

import tensorflow as tf
sess = tf.InteractiveSession() # 创建session，每个session数据和运算独立
x = tf.placeholder(tf.float32, [None, 784]) # session输入的数据尺寸(数据类型，[条数，维数])

W = tf.Variable(tf.zeros([784, 10])) # tf.Variable是存于内存中的张量，独立于session，用于存放学习的参数
b = tf.Variable(tf.zeros([10]))

y = tf.nn.softmax(tf.matmul(x, W) + b) # tf.nn包含大量神经网络算法组件

y_ = tf.placeholder(tf.float32, [None, 10]) # 真实分布
cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))

train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy) # 梯度下降最小化熵

tf.global_variables_initializer().run()

for i in range(1000):
    batch_xs, batch_ys = mnist.train.next_batch(100) # 一次给100组随机样本
    train_step.run({x: batch_xs, y_: batch_ys})

correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1)) # 预测值与真实值一一比对形成一个布尔矩阵

accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32)) # tf.cast数据转换，转为float32型

print(accuracy.eval({x: mnist.test.images, y_: mnist.test.labels})) # tensor.eval() 测试集走一遍到accuracy的计算流，得到测试集上的精准度
